import PIL.Image as Image

from utils import tile_raster_images

image = Image.fromarray(
    tile_raster_images(
        X=classifier.hiddenLayer.W.get_value(borrow=True).T,
        img_shape=(28, 28),
        tile_shape=(23, 23),
        tile_spacing=(1, 1)
    )
)
image.save('epoch_%i.png' % epoch)