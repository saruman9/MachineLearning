#-*- coding: utf-8 -*-

import numpy as np

#np.set_printoptions(linewidth=2000, threshold='nan')

"""
with open('data/newsgrouplabels.txt', 'r') as f:
    newsgrouplabels = f.read().split('\n')
with open('data/vocabulary.txt', 'r') as f:
    vocabulary = f.read().split('\n')
"""

#Чтение из файлов
newsgrouplabels = np.genfromtxt('data/newsgrouplabels.txt', dtype='S')
vocabulary = np.genfromtxt('data/vocabulary.txt', dtype='S')
train_data = np.genfromtxt('data/train.data', dtype=int)
train_label = np.genfromtxt('data/train.label', dtype=int)

"""
print(newsgrouplabels)
print(vocabulary)
print(train_data)
print(train_label)
"""

#Подсчёт количества вхождения тем среди документов
newsgrouplabels = np.c_[newsgrouplabels, np.zeros(len(newsgrouplabels), dtype=int)] #расширяем на колонку количества
for i in train_label:
    newsgrouplabels[i-1, 1] = newsgrouplabels[i-1, 1].astype(int) + 1
#Подсчёт частоты появления каждой темы
newsgrouplabels = np.c_[newsgrouplabels, np.divide(newsgrouplabels[:, 1].astype(float),\
                                                   newsgrouplabels[:, 1].astype(int).sum())] #расширяем на колонку частоты
print(newsgrouplabels)

#Подсчёт количества вхождения кажого слова в различные темы
#Расширяем на 20 колонок (20 тем)
vocabulary_entry = np.c_[vocabulary, np.zeros(len(vocabulary), dtype=int)]
for i in range(19):
    vocabulary_entry = np.c_[vocabulary_entry, np.zeros(len(vocabulary), dtype=int)]

#Подсчёт
for x in train_data:
    num_of_group = train_label[x[0]-1] #определяем номер темы
    vocabulary_entry[x[1]-1, num_of_group] = vocabulary_entry[x[1]-1, num_of_group].astype(int) + x[2]

print(vocabulary_entry)

#Подсчёт частоты вхождения слова в тему
vocabulary_prob = np.copy(vocabulary_entry)
for ed_x,item_x in enumerate(vocabulary_prob.T[1:]):
    sum_of_words_in_groups = item_x.astype(float).sum()
    for ed_i, item_i in enumerate(item_x):
        if sum_of_words_in_groups != 0:
            vocabulary_prob[ed_i, ed_x+1] = item_i.astype(float) / sum_of_words_in_groups

print(vocabulary_prob)