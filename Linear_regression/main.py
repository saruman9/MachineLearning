#-*- coding: utf-8 -*-

import numpy as np
from numpy import linalg as LA
import matplotlib.pyplot as plt
from sklearn import linear_model


X = np.loadtxt("diabetes.data", skiprows=1, usecols=(0,1,2,3,4,5,6,7,8,9))
Y = np.loadtxt("diabetes.data", skiprows=1, usecols=(10,))

Xtrain, Xtest = X[:200], X[200:]
Ytrain, Ytest = Y[:200], Y[200:]

"""
for i in range(200):
	plt.plot(Xtrain[i], [Ytrain[i] for i in range(len(Xtrain[i]))], 'ro')
plt.show()
"""

print 'Данные Xtrain: ', Xtrain
print 'Данные Ytrain: ', Ytrain

Xbar = np.mean(Xtrain, axis=0)
Xstd = np.std(Xtrain, axis=0)
Ybar = np.mean(Ytrain)
Ytrain = Ytrain - Ybar
Xtrain = (Xtrain - Xbar) / Xstd

print 'Среднее арифметическое X: ', Xbar
print 'Среднеквадратичное отклонение X: ', Xstd
print 'Среднее арифметическое Y: ', Ybar
print 'Xtrain после нормирования: ', Xtrain
print 'Ytrain после нормирования: ', Ytrain

"""
lambda = [x for x in range(10000)]
fi = []

for i in lambda:
	np.append(fi, np.dot())
"""

regr = linear_model.LinearRegression()
regr.fit(Xtrain, Ytrain)
print 'Коэффициенты θ: ', regr.coef_


###############################################################################
# Compute paths

alphas = [i for i in range(10000)]
teta_10 = []
clf = linear_model.Ridge(fit_intercept=False)

clf.set_params(alpha=10)
clf.fit(Xtrain, Ytrain)
teta_10.append(clf.coef_)

coefs = []
error_train = []
for a in alphas:
    clf.set_params(alpha=a)
    clf.fit(Xtrain, Ytrain)
    coefs.append(clf.coef_)
    Yhat = Ybar + np.dot((Xtrain - Xbar) / Xstd, clf.coef_)
    #error_train.append(np.dot(Ytrain - np.dot(Xtrain, clf.coef_).transpose(), Ytrain - np.dot(Xtrain, clf.coef_)))
    error_train.append(LA.norm(Ytrain - np.dot(Xtrain, clf.coef_)) / LA.norm(Ytrain))

###############################################################################
# Display results

ax = plt.gca()
ax.set_color_cycle(['b', 'r', 'g', 'c', 'k', 'y', 'm'])

ax.plot(alphas, coefs)
ax.set_xscale('log')
plt.xlabel('lambda (log)')
plt.ylabel('teta')
plt.title('Ridge coefficients as a function of the regularization')
plt.axis('tight')
plt.show()

print 'Коэффициенты при λ = 10: ', np.array(teta_10)


Xbar = np.mean(Xtest, axis=0)
Xstd = np.std(Xtest, axis=0)
Ybar = np.mean(Ytest)
Ytest = Ytest - Ybar
Xtest = (Xtest - Xbar) / Xstd

coefs = []
error_test = []
for a in alphas:
    clf.set_params(alpha=a)
    clf.fit(Xtest, Ytest)
    Yhat = Ybar + np.dot((Xtest - Xbar) / Xstd, clf.coef_)
    #coefs.append(clf.coef_)
    #error_test.append(np.dot(Ytest - np.dot(Xtest, clf.coef_).transpose(), Ytest - np.dot(Xtest, clf.coef_)))
    #error_test.append(Ybar + np.dot((Xtest - Xbar) / Xstd, clf.coef_))
    error_test.append(LA.norm(Ytest - np.dot(Xtest, clf.coef_)) / LA.norm(Ytest))

ax = plt.gca()
ax.set_color_cycle(['g', 'b'])

ax.plot(alphas[10:100], error_train[10:100], 'go-', label='Train data')
ax.plot(alphas[10:100], error_test[10:100], 'bo-', label='Test data')
ax.legend(loc='upper right', shadow=True)
ax.set_xscale('log')
plt.xlabel('lambda (log)')
plt.ylabel('error')
plt.title('Errors')
plt.axis('tight')
plt.show()
